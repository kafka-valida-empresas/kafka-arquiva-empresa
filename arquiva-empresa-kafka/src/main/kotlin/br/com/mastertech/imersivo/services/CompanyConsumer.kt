package br.com.mastertech.imersivo.services

import br.com.mastertech.imersivo.kafka.models.Company
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import java.io.FileWriter

@Component
class CompanyConsumer {
    @KafkaListener(topics=["chrislucas-biro-2"],  groupId = "chrisluccas2")
    fun listener(@Payload company: Company) {
        println("Saving Company: $company")
        writeFile(company)
    }

    private fun writeFile(company: Company) {
        FileWriter("raw/log.csv", true).use {
            it.write("$company\n")
        }
    }

}